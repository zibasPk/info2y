begin_problem(TPTP_Problem).

list_of_descriptions.
name({*Esercizio*}).
author({*NULL*}).
status(unsatisfiable).
description({*Il club dei barbieri*}).
date({*NULL*}).
end_of_list.

list_of_symbols.
functions[(cesare,0), (guido,0), (lorenzo,0), (petruccio,0)].
predicates[(tagliato_capelli,2)].
end_of_list.

list_of_formulae(axioms).

formula(forall([X,Y],implies(tagliato_capelli(X,Y),forall([Z],tagliato_capelli(Z,X))))).

formula(tagliato_capelli(guido,cesare)).

end_of_list.

list_of_formulae(conjectures).

formula(tagliato_capelli(petruccio,lorenzo)).

end_of_list.

list_of_settings(SPASS).
{*
set_flag(Sorts,0).
set_flag(Splits,0).
set_flag(Select,0).
%set_flag(RFMRR,0).
%set_flag(RBMRR,0). 
set_flag(RObv,0).
set_flag(RCon,0).
set_flag(RUnC,0).
set_flag(RAED,0).
set_flag(CNFRenaming,0).
set_flag(SatInput,0).
set_flag(DocProof,1).
*}
end_of_list.

end_problem.