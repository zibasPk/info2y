// Bosetti Luca matricola 830850
// Prova in itinere 1 AA 2016/2017
// programma IJVM che per un numero a scelta di valori interi immessi da tastiera calcola
// e stampa a video minimo, massimo, media, ricorrenze del minimo, ricorrenze del massimo
// che funziona correttamente solo per valori interi immessi da 0 a 127 la cui somma è <= 127.
// E' stato implementato un sistema di controllo per evitare di immettere dati errati
// o che non rispettano le specifiche di cui sopra e bloccare il programma.
// I numeri vanno immessi in sequenza seguiti da virgola, mentre l'ultimo numero che si 
// vuole immettere va seguito dal tasto ENTER. Il programma conta i numeri man mano che li si
// inserisce e poi calcola quanto richiesto. Per ciascun numero inserito il programma fa il
// controllo di non superamento del valore somma parziale che deve essere <= 127. Per far questo
// ho dovuto impostare i confronti cifra per cifra per evitare gli "out of range 127". Se il numero 
// è scartato viene visualizzato seguito da tante "x" (cod. ASCII 120) quante sono le cifre del 
// numero anche se l'intenzione più fine era quello di cancellarlo, mandando in output altrettanti 
// BACKSPACE (cod. ASCII 8) uno per ogni cifra. C'ho provato, ma probabilmente per questo serve 
// uno stream di output in qualche modo attivo. Comunque i numeri scartati seguiti dalle x sullo 
// schermo sono estranei al computo finale. 

.constant
	OBJREF 0   //costante per le chiamate ai vari metodi
.end-constant

.main
.var
	min			//minimo
	max			//massimo
	sum			//somma
	lim			//limite da non superare con la somma: 127 all'inizio, decrescente poi
	avg			//media
	tot			//usato per il calcolo e la corretta approssimazione della media all'int sup. o inf.
	nmin		//ricorrenze del ninomo	
	nmax		//ricorrenze del massimo
	counter		//contatore (unica variabile contatore usata in 2 contesti diversi nel programma)
	n			//contatore di quanti numeri validi sono immessi
	unita		//unità inserite (da controllare)
	decine		//decine inserite (da controllare)
	centinaia	//centinaia inserite (da controllare)
	cifre_vis   //contatore cifre mandate in stampa eventualmente da cancellare
.end-var

		BIPUSH 101
		OUT			//stampa "e"
		BIPUSH 108
		OUT			//stampa "l"
		BIPUSH 101
		OUT			//stampa "e"
		BIPUSH 110	
		OUT			//stampa "n"
		BIPUSH 99
		OUT			//stampa "c"	
		BIPUSH 111
		OUT			//stampa "o"	
		BIPUSH 58
		OUT			//stampa ":"	
		BIPUSH 32
		OUT			//stampa " "	
		BIPUSH 123
		OUT			//stampa "{"
		BIPUSH 32
		OUT			//stampa " "	
	
		BIPUSH 127		//imposto la somma massima dei valori inseribile
		ISTORE lim
			
		BIPUSH 0
		ISTORE counter  //azzera il contatore dei numeri validi immessi
acqNum:	
		BIPUSH 0
		DUP
		DUP
		DUP
		DUP
		ISTORE n  	    	// azzera il contatore dei numeri in acquisizione
		ISTORE centinaia	// azzera le centinaia del numero in acquisizione
		ISTORE decine		// azzera le decine del numero in acquisizione
		ISTORE unita		// azzera le unità del numero in acquisizione
		ISTORE cifre_vis	// azzera le cifre visualizzate del numero in acquisizione
leggi1:	
		IN				// legge un carattere potenziale proveniente dalla tastiera
		DUP				// duplico per il confronto IFEQ
		IFEQ elimina1	// se il carattere = 0 (nessun tasto premuto) elimina
		DUP				// se il codice carattere <> 0 duplica carattere
		BIPUSH 48
		ISUB			
		IFLT elimina1	// se il codice carattere < 48 (numero < 0) elimina il carattere
		DUP		
		BIPUSH 57
		SWAP	
		ISUB
		IFLT elimina1	// se il codice carattere > 57 (numero > 9) elimina il carattere
		DUP
		BIPUSH 48
		ISUB
		ISTORE unita 	// lo salvo nelle unità (e ne rimane una copia nello stack)
		ILOAD unita		
		BIPUSH 48
		IADD		 	// sullo stack c'è un numero da 1 a 9 (valido) e lo duplico
		OUT			 	// mando a video il numero corrispondente al tasto premuto
		IINC cifre_vis 1		
		BIPUSH 48
		ISUB
		IFEQ vir_ent 	// se il primo carattere è 0 mi aspetto una virgola o un enter	
		GOTO leggi2	 	// posso leggere il secondo carattere 
elimina1:
		POP				// elimina il carattere non numerico dalla cima dello stack
		GOTO leggi1		// prova a leggere un altro primo carattere numerico valido

leggi2: 
		IN				// legge un carattere potenziale proveniente dalla tastiera
		DUP				// duplico per il confronto IFEQ
		IFEQ elimina2	// se il codice carattere = 0 (nessun tasto premuto) elimina carattere
		DUP				// se il codice carattere <> 0 duplica carattere
		BIPUSH 44		
		ISUB
		IFEQ virgola	// se il carattere è una virgola vai alla label virgola
		DUP		
		BIPUSH 10		
		ISUB
		IFEQ enter		// se il carattere è una enter vai alla label enter
		DUP		
		BIPUSH 48
		ISUB			
		IFLT elimina2	// se il codice carattere < 48 elimina il carattere non numerico
		DUP		
		BIPUSH 57
		SWAP	
		ISUB	
		IFLT elimina2	// se il codice carattere > 57 elimina il carattere non numerico
		BIPUSH 48
		ISUB
		ILOAD unita  	// carico il numero delle unità 
		ISTORE decine 	// e lo salvo nelle decine (e ne rimane una copia nello stack)
		DUP		
		ISTORE unita	// salvo il secondo numero come unità
		BIPUSH 48
		IADD
		OUT	 			// mando a video il numero corrispondente al tasto premuto
		IINC cifre_vis 1	
		GOTO leggi3	 	// posso leggere il terzo carattere 
elimina2:
		POP	            // elimina il carattere non numerico dalla cima dello stack
		GOTO leggi2		// prova a leggere un altro secondo carattere numerico valido
leggi3: 
		IN				// legge un carattere potenziale proveniente dalla tastiera
		DUP				// duplico per il confronto IFEQ
		IFEQ elimina3	// se il codice carattere = 0 (nessun tasto premuto) elimina carattere
		DUP				// se il codice carattere <> 0 duplica carattere
		BIPUSH 44		
		ISUB
		IFEQ virgola	// se il carattere è una virgola vai alla label virgola
		DUP		
		BIPUSH 10		
		ISUB
		IFEQ enter		// se il carattere è una enter vai alla label enter
		DUP		
		BIPUSH 48
		ISUB			
		IFLT elimina3	// se il codice carattere < 48 elimina il carattere non numerico
		DUP		
		BIPUSH 57
		SWAP	
		ISUB	
		IFLT elimina3	// se il codice carattere > 57 elimina il carattere non numerico
		BIPUSH 48
		ISUB
		DUP
		ILOAD decine  	// carico il numero delle unità 
		ISTORE centinaia 	// e lo salvo nelle decine (e ne rimane una copia nello stack)
		ILOAD unita  	// carico il numero delle unità 
		ISTORE decine 	// e lo salvo nelle decine (e ne rimane una copia nello stack)
		ISTORE unita	// salvo il secondo numero come unità
		BIPUSH 48
		IADD
		OUT			 	// mando a video il numero corrispondente al tasto premuto
		IINC cifre_vis 1	
		GOTO vir_ent	// posso ora leggere il quarto carattere (virgola o enter)
		
elimina3:
		POP				// elimina il carattere non numerico dalla cima dello stack
		GOTO leggi3		// prova a leggere un altro terzo carattere numerico valido

vir_ent:
		IN				// legge i possibili caratteri virgola o enter provenienti dalla tastiera
		DUP				// duplico per il confronto IFEQ
		IFEQ elimina4	// se il codice carattere = 0 (nessun tasto premuto) elimina carattere
		DUP				// se il codice carattere <> 0 duplica carattere
		BIPUSH 44		
		ISUB
		IFEQ virgola	// se il carattere è una virgola vai alla label virgola
		DUP		
		BIPUSH 10		
		ISUB
		IFEQ enter		// se il carattere è una enter vai alla label enter
elimina4:		
		POP		
		GOTO vir_ent

virgola:		
		POP								// elimina il carattere in esubero dalla cima dello stack
		LDC_W OBJREF	
		ILOAD centinaia    	
		ILOAD decine
		ILOAD unita
		ILOAD lim
		ILOAD cifre_vis
    	INVOKEVIRTUAL controllaNumero 	// controlla che il numero inserito sia regolare
										// e restituisce il nuovo limite se lo è e -1 se non lo è 
		DUP
		IFLT back_vir					// se il valore restituito è -1 riacquisire un altro numero
		IINC counter 1					
		ISTORE lim						// altrimenti salva il nuovo limite (valore max inseribile)
		GOTO away_vir
	 back_vir:	
		POP
		GOTO acqNum
	 away_vir:	
		LDC_W OBJREF					// dalle cifre inserite, il numero corretto viene composto e lasciato
		ILOAD centinaia    				// impliato sullo stack al termine dell'esecuzione di componiNumero
		ILOAD decine
		ILOAD unita
    	INVOKEVIRTUAL componiNumero
		
		BIPUSH 44						// stampa di una virgola e di uno spazio di separazione dopo il num inserito
		OUT				//stampa ","
		BIPUSH 32
		OUT				//stampa " "	
		GOTO acqNum						//acquisizione del prossimo numero

enter: 
		POP								// elimina il carattere in esubero dalla cima dello stack
		LDC_W OBJREF	
		ILOAD centinaia    	
		ILOAD decine
		ILOAD unita
		ILOAD lim
		ILOAD cifre_vis
		INVOKEVIRTUAL controllaNumero	//controlla che il numero inserito sia regolare
										// e restituisce il nuovo limite se lo è e -1 se non lo è 
		DUP
		IFLT back_ent					// se il valore restituito è -1 riacquisire un altro numero
		IINC counter 1	
		ISTORE lim						// altrimenti salva il nuovo limite (valore max inseribile)
		GOTO away_ent
	 back_ent:	
		POP
		GOTO acqNum
	 away_ent:	
		LDC_W OBJREF					// dalle cifre inserite, il numero corretto viene composto e lasciato
		ILOAD centinaia    				// impliato sullo stack al termine dell'esecuzione di componiNumero
		ILOAD decine
		ILOAD unita
    	INVOKEVIRTUAL componiNumero
		GOTO fineAcq					//fine acquisizione numeri, proseguo con stampe di chiusura 1 riga

fineAcq:
		BIPUSH 32
		OUT					
		BIPUSH 125
		OUT					
		BIPUSH 32		
		OUT					
		BIPUSH 45
		OUT					//stampe di chiusura 1 riga
		BIPUSH 62
		OUT					
		BIPUSH 10
		OUT					
		
		ILOAD counter // carica il numero nel contatore di numeri inseriti
		ISTORE n	  // passa tale numero a n
		
		// a questo punto il programma ha terminato l'acquisizione e controllo dei dati, ha i numeri inseriti
		// impilati sullo stack e parte con i calcoli consumando i numeri da cima a fondo e man mano calcolando 
		// min, max, sum, nmin e nmax (sum è la somma parziale dei numeri acquisiti).
	
		DUP			
		DUP
		DUP
		ISTORE min
		ISTORE max
		ISTORE sum
		BIPUSH 1
		DUP
		DUP
		ISTORE nmin
		ISTORE nmax
		ILOAD n
		SWAP
		ISUB			// e fino a qui ho inserito i dati aggiornati del primo numero sullo stack
		ISTORE counter 	// counter = n-1. Counter viene ora utilizzata in modo decrescente per contare
						// i numeri ancora da computare, senza inizializzare un'altra variabile.
		POP				// eliminazione del primo numero sullo stack già considerato
		

		// in questo ciclo da loop ad end loop ripetuto n-1 volte il programma aggiorna min, max, sum, nmin e nmax
		// con le opportune condizioni	

loop:		
			ILOAD counter
			IFEQ calc_avg	//quando counter è a valore zero passa al calcolo della media.
		ctrl_min:	
				DUP
				ILOAD min		
				ISUB
				DUP			
				IFEQ inc_nmin
				IFLT new_min 
				GOTO ctrl_max
		inc_nmin:	
				POP
				BIPUSH 1
				ILOAD nmin
				IADD
				ISTORE nmin
				GOTO ctrl_max
		new_min:
				DUP
				ISTORE min
				BIPUSH 1
				ISTORE nmin
				GOTO end_loop
		ctrl_max:	
				DUP
				ILOAD max
				SWAP
				ISUB
				DUP
				IFEQ inc_nmax
				IFLT new_max 
				GOTO end_loop
		inc_nmax:	
				POP	
				BIPUSH 1
				ILOAD nmax
				IADD
				ISTORE nmax				
				GOTO end_loop
		new_max:	
				DUP
				ISTORE max
				BIPUSH 1
				ISTORE nmax
				GOTO end_loop
		end_loop:
				ILOAD counter
				BIPUSH 1
				ISUB
				ISTORE counter
				ILOAD sum					
				IADD
				ISTORE sum	
				GOTO loop

		// in questa parte del programma viene calcolata la media. Conosco sum, la somma totale, e conosco
		// n (quanti numeri ho inserito), variabile immutata dall'inserimento precedente. Ho però bisogno
		// della variabile ausiliaria tot, da incrementare avg unità alla volta con inc_avg. Quando la variabile tot ha
		// superato sum, avg è la media approssimata per eccesso o la media+1. A questo punto 	


		calc_avg: 			// inizializzazione variabili in gioco
				BIPUSH 0
				ISTORE tot
				BIPUSH -1
			  	ISTORE avg
				BIPUSH 0				
				ILOAD n	
				ISUB
				ISTORE tot			
		inc_avg:			// calcolo avg per eccesso 
				IINC avg 1	
				ILOAD tot 
				ILOAD n
				IADD
				DUP				
				ISTORE tot		  		
				ILOAD sum
				ISUB			
				IFLT inc_avg
		arr_avg:			// dal confronto (sum-(tot-n)) e (tot-sum)
				ILOAD tot	// calcolo (sum-(tot-n))
				ILOAD n
				ISUB
				ILOAD sum
				SWAP
				ISUB
	
				ILOAD tot	// calcolo (tot-sum)
				ILOAD sum
				ISUB
			
				ISUB
				IFLT arr	// se (sum-(tot-n)) < (tot-sum) togli 1 ad avg
				GOTO print_a //prosegui con le stampe a video
			arr:
				IINC avg -1								 	

print_a: 				//stampa prima riga
		BIPUSH 65
		OUT
		BIPUSH 41
		OUT
		BIPUSH 32
		OUT
		BIPUSH 77
		OUT		
		BIPUSH 73
		OUT		
		BIPUSH 78
		OUT
		BIPUSH 58
		OUT
		BIPUSH 32
		OUT
		LDC_W OBJREF
    	ILOAD min
    	INVOKEVIRTUAL stampaNumero
		POP
		BIPUSH 44
		OUT
		BIPUSH 32
		OUT
		BIPUSH 77
		OUT		
		BIPUSH 65
		OUT		
		BIPUSH 88
		OUT
		BIPUSH 58
		OUT
		BIPUSH 32
		OUT
		LDC_W OBJREF
    	ILOAD max
    	INVOKEVIRTUAL stampaNumero
		POP
		BIPUSH 10
		OUT
print_b:					//stampa seconda riga
		BIPUSH 66
		OUT
		BIPUSH 41
		OUT
		BIPUSH 32
		OUT
		BIPUSH 65
		OUT		
		BIPUSH 86
		OUT		
		BIPUSH 71
		OUT
		BIPUSH 58
		OUT
		BIPUSH 32
		OUT
		LDC_W OBJREF
    	ILOAD avg
    	INVOKEVIRTUAL stampaNumero
		POP
		BIPUSH 10
		OUT
print_c: 					//stampa terza riga
		BIPUSH 67
		OUT
		BIPUSH 41
		OUT
		BIPUSH 32
		OUT
		BIPUSH 78
		OUT	
		BIPUSH 77
		OUT		
		BIPUSH 73
		OUT		
		BIPUSH 78
		OUT
		BIPUSH 58
		OUT
		BIPUSH 32
		OUT
		LDC_W OBJREF
    	ILOAD nmin
    	INVOKEVIRTUAL stampaNumero
		POP   		
		BIPUSH 44
		OUT
   		BIPUSH 32
		OUT
		BIPUSH 78
		OUT	
		BIPUSH 77
		OUT		
		BIPUSH 65
		OUT		
		BIPUSH 88
		OUT
		BIPUSH 58
		OUT
		BIPUSH 32
		OUT
		LDC_W OBJREF
    	ILOAD nmax
    	INVOKEVIRTUAL stampaNumero
		POP		
		BIPUSH 10		
		OUT
	HALT
.end-main

///////////////////////////////////////////////////// END MAIN ///////////////////////////////////////////////

.method controllaNumero(c,d,u,lim,cifre) // scopo di questo metodo è di controllare che il numero digitato
   .var									 // sia regolare (minore del limite massimo corrente), di contare
	 lim_c								 // le cifre stampate a video per la eventuale successiva cancellazione,
	 lim_d								 // di mandare a video il segno di cancellazione del numero non regolare
	 lim_u								 // immesso, in caso di accettazione restituire al chiamante il nuovo
   .end-var								 // limite massimo corrente ammissibile, in caso di non accettazione
										 // restituire sullo stack il numero negativo -1.

			LDC_W OBJREF				 // queste prime tre chiamate estraggono le cifre dal limite corrente <127
    		ILOAD lim					 // per poter effettuare le somme senza "out of range"
    	INVOKEVIRTUAL estraiCentinaia
        	ISTORE lim_c
			LDC_W OBJREF
    		ILOAD lim
    	INVOKEVIRTUAL estraiDecine
			ISTORE lim_d
			LDC_W OBJREF
			ILOAD lim
    	INVOKEVIRTUAL estraiUnita
			ISTORE lim_u
					
			ILOAD lim_u					// questi vari step sommano tra loro unità, decine e centinaia e considerano
			ILOAD u						// i riporti all'unità superiore, come per i conti "a mano" onde evitare "l'out
			ISUB						// of range" (u=unità, d=decine, c=centinaia)
			DUP
			IFLT step1
			ISTORE u			
			GOTO step2			
	  step1:
			BIPUSH 10
			IADD			
			ISTORE u
			ILOAD lim_d
			BIPUSH 1
			ISUB
			ISTORE lim_d
 	  step2:
			ILOAD lim_d
			ILOAD d
			ISUB
			DUP 
			IFLT step3
			ISTORE d			
			GOTO step4
	  step3:
			BIPUSH 10
			IADD			
			ISTORE d
			ILOAD lim_c
			BIPUSH 1
			ISUB
			ISTORE lim_c
	  step4:
			ILOAD lim_c
			ILOAD c
			ISUB
			DUP 
			IFLT erase_pop			// a questo punto il numero inserito ha superato il limite
			ISTORE c
		LDC_W OBJREF
    		ILOAD c
			ILOAD d
			ILOAD u
    	INVOKEVIRTUAL componiNumero	// in caso di controllo positivo compone il numero da
									// restituire al chiamante come nuovo limite
			GOTO fine				
		erase_pop: 
			POP						// pop necessario proveniendo dallo step 4 per pulizia stack
		erase:
			ILOAD cifre
			IFEQ no
			BIPUSH 120				//carattere "x" (BACKSPACE = 8 funzionante era più desiderabile)
			OUT	
			ILOAD cifre
			BIPUSH 1
			ISUB
			ISTORE cifre
			GOTO erase
		no:
			BIPUSH 32
			OUT						//	stampa " " dopo le "x"				
			BIPUSH -1				// in caso di controllo negativo restituisce -1 al chiamante
		fine:	
			IRETURN 
.end-method
							// il metodo estraiCentinaia determina se il numero è maggiore o uguale 
							// siccome i valori possibili dei numeri del programma sono da 0 a 127,						
							// di 100 (100-127) restituendo 1, o minore di 100 (0-99) restituendo 0. 	

.method estraiCentinaia(ce)
  .var
    cen
  .end-var
		BIPUSH 1
        ISTORE cen
		BIPUSH 100 
		ILOAD ce       
		ISUB
        IFLT ENDx
        BIPUSH 0
		ISTORE cen
	ENDx:
		ILOAD cen
		IRETURN 
.end-method
							// il metodo estraiDecine ad ogni ciclo decrementa di 10 il numero entrante
							// ed incrementa di 1 le decine. Quando il numero diventa negativo il metodo
							// restituisce al main il valore delle decine presente in dec.	
.method estraiDecine(de)
  .var
    dec
    yCorr
  .end-var
        BIPUSH 0
        ISTORE dec
        ILOAD de
        ISTORE yCorr
	RIPETIy: 
        ILOAD yCorr
        BIPUSH 10
        ISUB
        IFLT ENDy
        IINC dec 1
        IINC yCorr -10
        GOTO RIPETIy
    TOGLI_10:
		ILOAD dec	
		BIPUSH 10
        ISUB
		GOTO ENDyy
	ENDy:
		BIPUSH 9
		ILOAD dec
		ISUB
		IFLT TOGLI_10 
		ILOAD dec
	ENDyy:	
		IRETURN
.end-method
							// il metodo estraiUnita ad ogni ciclo decrementa di 10 il numero entrante
							// Quando il numero diventa negativo il metodo restituisce al main il valore 
							// in zCorr (unità) presente in memoria prima dell'ultima sottrazione.	
.method estraiUnita(un)
  .var
    zCorr
  .end-var
        ILOAD un
        ISTORE zCorr
	RIPETIz: 
        ILOAD zCorr
        BIPUSH 10
        ISUB
        IFLT ENDz
        IINC zCorr -10
        GOTO RIPETIz
	ENDz:
		ILOAD zCorr
		IRETURN 
.end-method

.method componiNumero(c,d,u) //Date centinaia, unità e decine restituisce in uscita
   .var						 //il numero somma
  	 num
   .end-var		
			ILOAD u
       		ISTORE num 						
	ciclo:	
			ILOAD d 
      			IFEQ step
			BIPUSH 10
			ILOAD num
			IADD
			ISTORE num
			BIPUSH -1
			ILOAD d			
			IADD
			ISTORE d
			GOTO ciclo
    	step: 
			ILOAD c
			IFEQ end
			BIPUSH 100
			ILOAD num
			IADD	
			ISTORE num
	end: 	
			ILOAD num
			IRETURN
.end-method

.method stampaNumero(num)  	// dato un numero in entrata stampa a video nell'ordine:
   .var						// centinaia (se >0), decine e unità								
  	 u
	 d
   	 c
   .end-var
       		LDC_W OBJREF
    		ILOAD num
    	INVOKEVIRTUAL estraiCentinaia
        	ISTORE c
			LDC_W OBJREF
    		ILOAD num
    	INVOKEVIRTUAL estraiDecine
			ISTORE d
			LDC_W OBJREF
			ILOAD num
    	INVOKEVIRTUAL estraiUnita
			ISTORE u
			ILOAD c
			IFEQ lbl1
			ILOAD c
			BIPUSH 48
			IADD		
			OUT
		lbl1:	
			ILOAD c
			ILOAD d
			IADD
			IFEQ lbl2
			ILOAD d
			BIPUSH 48
			IADD
			OUT
		lbl2:
			ILOAD u
			BIPUSH 48
			IADD
			OUT		
		IRETURN 
.end-method
